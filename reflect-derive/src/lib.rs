#[macro_use]
extern crate quote;
extern crate proc_macro;
extern crate syn;

use proc_macro::TokenStream;

#[proc_macro_derive(Reflect)]
pub fn reflect(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();

    let gen = impl_reflect(&ast);

    gen.parse().unwrap()
}

fn impl_reflect(ast: &syn::DeriveInput) -> quote::Tokens {
    let name = &ast.ident;

    let field_names = match ast.body {
        syn::Body::Enum(ref variants) => variants.iter().map(|x| &x.ident).collect(),
        syn::Body::Struct(ref variant_data) => match get_field_names(variant_data) {
            Some(vec) => vec,
            None => Vec::new(),
        },
    };

    quote! {
        impl Reflect for #name {
            fn name(&self) -> &str {
                stringify!(#name)
            }

            fn fields(&self) -> Vec<&str> {
                vec![#(stringify!(#field_names)),*]
            }
        }
    }
}

fn get_field_names(variant: &syn::VariantData) -> Option<Vec<&syn::Ident>> {
    match variant {
        &syn::VariantData::Struct(ref fields) => {
            Some(fields.iter().map(|x| x.ident.as_ref().unwrap()).collect())
        },
        &syn::VariantData::Tuple(_) => None,
        &syn::VariantData::Unit => None
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
