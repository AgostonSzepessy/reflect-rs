#[macro_use]
extern crate reflect_derive;

trait Reflect {
    fn name(&self) -> &str;
    fn fields(&self) -> Vec<&str>;
}

#[cfg(test)]
mod tests {
    use super::Reflect;

    #[derive(Reflect)]
    struct Test {
        x: i32,
        y: u32,
    }

    impl Test {
        fn new() -> Self {
            Test {
                x: 0,
                y: 0,
            }
        }
    }

    #[derive(Reflect)]
    enum TestEnum {
        Field1,
        Field2,
    }

    #[derive(Reflect)]
    struct TestTupleStruct(i32, u32);

    #[test]
    fn test_name() {
        let test = Test::new();
        assert_eq!("Test", test.name());
    }

    #[test]
    fn test_fields() {
        let test = Test::new();
        assert_eq!(vec!["x", "y"], test.fields());
    }

    #[test]
    fn test_enum_name() {
        let test_enum = TestEnum::Field1;
        assert_eq!("TestEnum", test_enum.name());
    }

    #[test]
    fn test_enum_fields() {
        let test_enum = TestEnum::Field1;
        assert_eq!(vec!["Field1", "Field2"], test_enum.fields());
    }

    #[test]
    fn test_tuple_struct_name() {
        let test = TestTupleStruct(0, 0);
        assert_eq!("TestTupleStruct", test.name());
    }

    #[test]
    fn test_tuple_struct_fields() {
        let test = TestTupleStruct(0, 0);
        assert_eq!(Vec::<&str>::new(), test.fields());
    }
}
